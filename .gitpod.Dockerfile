FROM gitpod/workspace-full-vnc
                    
USER gitpod

RUN sudo apt-get -q update
RUN sudo apt-get -y upgrade

# Original SBCL install below has been commented-out and replaced with
# a Roswel installation instead (see below)
#
#RUN sudo apt-get install -y cl-clx-sbcl sbcl sbcl-doc sbcl-source rlwrap
#RUN curl -o /tmp/ql.lisp http://beta.quicklisp.org/quicklisp.lisp
#RUN sbcl --no-sysinit --no-userinit --load /tmp/ql.lisp \
#       --eval '(quicklisp-quickstart:install :path "~/.quicklisp")' \
#       --eval '(ql:add-to-init-file)' \
#       --quit

# Install Roswell
RUN curl -L https://github.com/roswell/roswell/releases/download/v21.05.14.109/roswell_21.05.14.109-1_amd64.deb -o /tmp/roswell.deb
RUN sudo dpkg -i /tmp/roswell.deb

# now that we have roswell installed, use it to setup sbcl
RUN ros install sbcl
RUN ros use sbcl

# install dependencies needed for VS Code (LSP) plugin
# https://marketplace.visualstudio.com/items?itemName=ailisp.commonlisp-vscode
#RUN ros install ailisp/linedit
#RUN ros install ailisp/prepl
#RUN ros install ailisp/cl-lsp

