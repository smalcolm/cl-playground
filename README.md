# cl-playground

Playground for Common Lisp programs

## Open in GitPod

https://github.io/#https://gitlab.com/smalcolm/cl-playground

On the command line use $ rlwrap sbcl to open the REPL with arrow keys and backspace being enabled.

