;;;; hello-world.lisp - test SBCL on gitpod

(defun greet (&optional (name "Null"))
  "greet : string -> string
  create hello 'name' string. If name is not a string then greet stranger"
  (if (typep name 'string)
    (concatenate 'string "Hello " name)
    (format t "Howdy Stranger!")))

;; run this program from gitpod terminal:
;;
;; 1. in terminal, start repl using $ rlwrap sbcl
;; 2. in sbcl load using (load #p"/workspace/cl-playground/hello-world.lisp")